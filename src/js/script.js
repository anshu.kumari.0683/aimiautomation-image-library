/**
 * Author :: Anshu
 * Represents an Image Gallery for lazy loading images.
 */

class ImageGallery {
  /**
   * Base URL for image sources.
   * @type {string}
   */
  imageBaseUrl = 'https://istraddle.com/';

  /**
   * Mode of image loading.
   * @type {string}
   */
  mode = 'limit';

  /**
   * Number of images to load per chunk for lazy loading.
   * @type {number}
   */
  chunkSize = 12;

  /**
   * Array containing chunks of images.
   * @type {Array}
   */
  imageChunks = [];

  /**
   * Index of the currently loaded image chunk.
   * @type {number}
   */
  chunkIndex = 0;

  /**
   * Constructor function.
   * @param {Element} container - The container element for the image gallery.
   */
  constructor(container) {
    this.container = container;
  }

  /**
   * Set the mode of image loading.
   * @param {string} mode - The mode to set ('limit' or 'recursive').
   */
  setMode(mode) {
    this.mode = mode;
  }

  /**
   * Static method to asynchronously load an image from a given source URL.
   * @param {string} src - The source URL of the image.
   * @returns {Promise<string>} A promise that resolves with the source URL of the loaded image.
   */
  static loadImage(src) {
    return new Promise((resolve, reject) => {
      const image = new Image();
      image.addEventListener('load', function () {
        resolve(src);
      });
      image.addEventListener('error', reject);
      image.src = src;
    });
  }

  /**
   * Chunk an array of images into smaller arrays for lazy loading.
   * @param {Array} array - The array of images to chunk.
   * @param {number} chunkSize - The size of each chunk.
   * @returns {Array} An array containing the chunked arrays of images.
   */
  chunkImages = (array, chunkSize) => {
    const result = [];
    for (let i = 0; i < array.length; i += chunkSize) {
      const chunk = array.slice(i, i + chunkSize);
      result.push(chunk);
    }
    return result;
  }

  /**
   * Fetch images from an API endpoint.
   * @returns {Promise<Array>} A promise that resolves with an array of fetched images.
   */
  fetchImages = async () => {
    const response = await fetch("https://aimiautomation-image-library.istraddle.com/images.json");
    return await response.json();
  };

  /**
   * Initialize the image gallery by fetching images and loading the first chunk.
   * @returns {Promise<void>}
   */
  async init() {
    const self = this;
    const images = await self.fetchImages();
    self.imagesCount = images.length;
    self.imageChunks = self.chunkImages(images, self.chunkSize);
    self.renderImages().then(function () {
    });
  }

  /**
   * Render images for the current chunk.
   * @returns {Promise<void>}
   */
  async renderImages() {
    const self = this;
    if (self.imageChunks) {
      if (self.imageChunks[self.chunkIndex]) {
        self.imageChunks[self.chunkIndex].forEach(function (image, index) {
          const imageUrl = `${self.imageBaseUrl}${image}`
          const imageElement = self.renderCard(imageUrl, index);
          observer.observe(imageElement);
        });
        self.chunkIndex++;
      } else {
        if (self.mode === 'recursive') {
          self.chunkIndex = 0;
          self.renderImages().then(function () {
          });
        } else {
          throw new Error('No more Images');
        }
      }
    }
  }

  /**
   * Render an image card.
   * @param {string} imageUrl - The URL of the image.
   * @param {number} index - The index of the image.
   * @returns {HTMLDivElement} The rendered image card element.
   */
  renderCard(imageUrl, index) {
    const card = document.createElement('div');
    card.classList.add('image-item', 'd-flex');
    card.setAttribute('data-image-url', imageUrl);
    card.innerHTML = `
      <div class="card w-100">
        <label class="image-item-label">
          <input value="${index}" id="checkbox-${index}" name="image-items[]" type="checkbox" class="image-item-input position-absolute ">
          <span class="image-item-loader placeholder-glow position-absolute w-100 h-100"> <span class="placeholder w-100 h-100"></span> </span>
          <div class="image-item-header position-absolute">
            <button class="btn btn-transparent image-item-maximize" data-bs-toggle="modal" data-bs-target="#imageMaximizedModal"></button>
          </div>
        </label>
      </div>
    `;
    this.container.appendChild(card);
    return card;
  };
}

/**
 * Image Builder needed by pageObserver.
 */
let imageBuilder;

/**
 * Lazy loader.
 */
let loader;

/**
 * Observer for images from image chunks.
 * @type {IntersectionObserver}
 */
const observer = new IntersectionObserver(
    (entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          const card = entry.target;
          const imageUrl = card.getAttribute("data-image-url");
          const checkbox = card.querySelector('input.image-item-input');
          ImageGallery.loadImage(imageUrl).then(function () {
            checkbox.style.backgroundImage = `url(${imageUrl})`;
            card.classList.add('loaded');
          })
          observer.unobserve(card);
        }
      });
    },
    {
      root: null,
      threshold: 0.01,
    }
);

/**
 * Lazy Load Observer.
 * @type {IntersectionObserver}
 */
const pageObserver = new IntersectionObserver(
    (entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          const element = entry.target;
          imageBuilder.renderImages().then(function () {
            // Something to do here when render images done
          }).catch(function (error) {
            loader.classList.add('d-none');
            pageObserver.unobserve(element);
          });
        }
      });
    },
    {
      root: null,
      rootMargin: '200px',
      threshold: [0, 0.5, 1] // callback triggers when target starts to become visible, is halfway visible, and fully visible
    }
);

/**
 * Wait until the DOM is ready before executing the provided function.
 * Initializes the image gallery, sets up event listeners, and handles image maximization.
 */
document.addEventListener('DOMContentLoaded', function () {
  const grid = document.querySelector("#aimi-image-grid");
  const preLoader = document.querySelector('.aimi-gallery-progress');
  loader = document.querySelector('#aimi-lazy-loader');

  // Expose image builder to window
  imageBuilder = new ImageGallery(grid);

  // Set the image gallery in recursive mode if specified in URL parameters
  if (getUrlParameter('mode') === 'recursive') {
    imageBuilder.setMode('recursive');
  }

  // Initialize the image gallery and observe the loader for lazy loading
  imageBuilder.init().then(function () {
    preLoader.classList.add('d-none');
    pageObserver.observe(loader);
  });

  // Image Maximizer
  const imageMaximizedModalEl = document.getElementById('imageMaximizedModal')
  imageMaximizedModalEl.addEventListener('show.bs.modal', event => {
    const modal = event.target;
    const imageContainer = modal.querySelector('.image-container');
    const loader = modal.querySelector('.spinner-border');
    imageContainer.innerHTML = '';
    const card = event.relatedTarget.closest('.image-item');
    // The URL of a higher resolution image (e.g., data-original-image-url)
    const imageUrl = card.getAttribute("data-image-url");
    ImageGallery.loadImage(imageUrl).then(function (response) {
      loader.classList.add('d-none');
      const img = document.createElement('img');
      img.classList.add('img-fluid', 'rounded');
      img.src = response;
      imageContainer.appendChild(img);
    })
  });

  // Clean maximize modal on hide
  imageMaximizedModalEl.addEventListener('hidden.bs.modal', event => {
    const modal = event.target;
    const imageContainer = modal.querySelector('.image-container');
    const loader = modal.querySelector('.spinner-border');
    imageContainer.innerHTML = '';
    loader.classList.remove('d-none');
  })
});