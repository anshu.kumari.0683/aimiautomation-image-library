/**
 * This file simply contains some helper function to enhance the assignment assessment
 * So please don't consider it as part of assignment
 */

function getUrlParameter(name) {
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  return urlParams.get(name);
}
