# Image Library (Aimi Automation)

## Getting Started

The project is deployed at the following link to facilitate review:
[Image Library Deployment](https://aimiautomation-image-library.istraddle.com/)

To test the library in infinite mode, access it via the following link:
[Infinite Mode Test](https://aimiautomation-image-library.istraddle.com/?mode=recursive)

For demonstration purposes, Library has a public method to activate recursive mod, which is getting activated in this link [Infinite Mode Test] from url parameter

```javascript
?mode=recursive
```
```javascript
imageBuilder.setMode('recursive');
```

## Source code

You have the option to view the source code either in the "src" folder within the repository or directly access it via the provided link.
https://gitlab.com/anshu.kumari.0683/aimiautomation-image-library/-/tree/main/src?ref_type=heads

## Distribution Code

The compiled project is contained within the "distribution" folder, as the name implies. You can simply download the
files from this folder and run them locally on your machine.
https://gitlab.com/anshu.kumari.0683/aimiautomation-image-library/-/tree/main/dist?ref_type=heads

## Platforms supported

The library has been tested on both mobile and desktop browsers that support the latest version of JavaScript. It
ensures a responsive layout where a maximum of 6 images are displayed in a row on very large screens. As the viewport
size decreases, the number of images per row adjusts accordingly: 5, 4, 3, 2, and 1. This ensures optimal viewing and
usability across various devices and screen sizes.

## Project Details

The library is designed to load a maximum of 12 (customizable in library code) images, constituting one page, at a time.
As you scroll down the page
and reach the end of the currently loaded set of images, the library dynamically loads another set of images. This
process continues until all images are loaded.

Additionally, images are loaded only when they come into the viewport, optimizing performance and ensuring a smooth
browsing experience. This approach enhances page loading speed and conserves bandwidth by loading images progressively
as the user interacts with the page.

## Code

Library is written in Vanilla JS, HTML5, Bootstrap, You can see all in src ( scss, js)
Bootstrap Components used -

1. Bootstrap modal to view images in original form, since images are small originially you can see the modal small
2. Bootstrap placeholder to show loader
3. Bootstrap progress bar while the initial api / page load in progress ( you can see that for a second but if will api
   will take time it can be visible easily)
4. Bootstrap grid structure to build the library with bootstrap cards

Grunt -
1. Used to concat js and css and minify them to one file

Js-
1. ImageLibrary class build as plugin and using the latest version available, e.g arrow functions, async etc

## Assignment notes

[Link shared](https://bitbucket.org/shankaran/image-library/src/main/)

## Image Credits

I have used images from this link, where you can generate random images from free
https://api-ninjas.com/api/randomimage

## Notes and Email Credits 
Using ChatGpt, Gemini to rephrase my English and R&D
