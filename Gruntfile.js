module.exports = function (grunt) {
  grunt.initConfig({
    concat: {
      options : {
        sourceMap :false
      },
      dist: {
        src: [
          'src/vendor/bootstrap-5.3.3-dist/js/bootstrap.bundle.js',
          "src/js/script.js"
        ],
        dest: 'dist/js/script.js'
      },
      html: {
        src: ["src/index.html"],
        dest: "dist/index.html",
      },
    },
    uglify: {
      js: {
        files: {
          "dist/js/script.js": "dist/js/script.js",
        },
      },
    },
    cssmin: {
      target: {
        files: {
          'dist/css/styles.css': [
            'src/vendor/bootstrap-5.3.3-dist/css/bootstrap.css',
            'src/css/styles.css',
          ]
        }
      }
    },
    sass: {
      layout: {
        options: {
          style: "compressed",
        },
        files: {
          "src/css/styles.css": "src/scss/styles.scss",
        },
      },
    }
  });

  grunt.loadNpmTasks("grunt-contrib-sass");
  grunt.loadNpmTasks("grunt-contrib-concat");
  grunt.loadNpmTasks("grunt-contrib-uglify");
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.registerTask("default", ["sass", "concat", "uglify", "cssmin"]);
};
